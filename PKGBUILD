# This pkgbuild is maintained by Echoa https://gitlab.com/echoa
# Arch maintainers for SDDM (which this pkgbuild was based from)
# Maintainer: Felix Yan <felixonmars@archlinux.org>
# Maintainer: Antonio Rojas <arojas@archlinux.org>
# Contributor: Andrea Scarpino <andrea@archlinux.org>

_pkgbase="sddm"
pkgname="$_pkgbase-wayland"
pkgver=0.19.0.9.b923ecc
pkgrel=1
pkgdesc='QML based X11 and Wayland display manager'
arch=(x86_64)
url='https://github.com/sddm/sddm'
license=(GPL)
depends=(qt5-declarative xorg-xauth xorg-server ttf-font weston)
makedepends=(extra-cmake-modules python-docutils qt5-tools git)
backup=('usr/share/sddm/scripts/Xsetup'
        'usr/share/sddm/scripts/Xstop'
        'etc/pam.d/sddm'
        'etc/pam.d/sddm-autologin'
        'etc/pam.d/sddm-greeter')
provides=("$_pkgbase" "$_pkgbase"-git sddm-wayland display-manager)
conflicts=("$_pkgbase" "$_pkgbase"-git)
source=($_pkgbase::"git+https://github.com/sddm/sddm.git#branch=develop"
        "10-wayland.conf"
        sddm.sysusers sddm.tmpfiles)
sha256sums=('SKIP'
            '72f2bd1814fb0a99ab3f216f2b653d301b7f6d791d600c7658804b8f47251c60'
            '9fce66f325d170c61caed57816f4bc72e9591df083e89da114a3bb16b0a0e60f'
            'db625f2a3649d6d203e1e1b187a054d5c6263cadf7edd824774d8ace52219677')

pkgver() {
    cd $_pkgbase
    #_ver="$(cat CMakeLists.txt | grep -m3 -e _VERSION_MAJOR -e _VERSION_MINOR -e _VERSION_PATCH | grep -o "[[:digit:]]*" | paste -sd'.')"
    #echo "${_ver}.r$(git rev-list --count HEAD).g$(git rev-parse --short HEAD)"
    git describe --tags --long | sed 's/^v//;s/-/./g'
}            

build() {
  cmake -B build -S $_pkgbase \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DCMAKE_INSTALL_LIBEXECDIR=/usr/lib/sddm \
        -DDBUS_CONFIG_DIR=/usr/share/dbus-1/system.d \
        -DDBUS_CONFIG_FILENAME=sddm_org.freedesktop.DisplayManager.conf \
        -DBUILD_MAN_PAGES=ON \
        -DUID_MAX=60513
  cmake --build build
}

package() {
  DESTDIR="$pkgdir" cmake --install build

  install -Dm644 sddm.sysusers "$pkgdir"/usr/lib/sysusers.d/sddm.conf
  install -Dm644 sddm.tmpfiles "$pkgdir"/usr/lib/tmpfiles.d/sddm.conf
  install -Dm644 10-wayland.conf "$pkgdir"/etc/sddm.conf.d/10-wayland.conf

  install -d "$pkgdir"/usr/lib/sddm/sddm.conf.d
  "$pkgdir"/usr/bin/sddm --example-config > "$pkgdir"/usr/lib/sddm/sddm.conf.d/default.conf
# Don't set PATH in sddm.conf
  sed -r 's|DefaultPath=.*|DefaultPath=/usr/local/sbin:/usr/local/bin:/usr/bin|g' -i "$pkgdir"/usr/lib/sddm/sddm.conf.d/default.conf
# Unset InputMethod https://github.com/sddm/sddm/issues/952
  sed -e "/^InputMethod/s/qtvirtualkeyboard//" -i "$pkgdir"/usr/lib/sddm/sddm.conf.d/default.conf
}
