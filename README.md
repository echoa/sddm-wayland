# SDDM-Wayland

Package Build filled with my experiments in running SDDM on Wayland

# Current Known issues

Running GreeterEnvironment=QT_WAYLAND_SHELL_INTEGRATION=layer-shell with weston doesnt work, it does with Kwin but using kwin results in some weird behaviors resulting from SDDM being treated as a window. If switching to using Kwin add the above greeterenvironment to the [general] section of 10-wayland.conf
